class PhoneNumber

  attr_reader :logger
  attr_accessor :client, :sanitized_results, :phone_numbers

  def initialize(phone_numbers, client, logger: Logger.new(STDOUT))
    @phone_numbers = phone_numbers
    @client = client
    @logger = logger
    @sanitized_results = []
  end

  # @return [String] STDOUT
  def parse_nummbers
    phone_numbers.each do |number|
      sanitized_results << country_code(number) + ' ' + sanitized(number)
    end
    sanitized_results
  end

  private

  # @param type [String]
  # @return [String]
  def country_code(number)
    phone_number = client.lookups.phone_numbers(number)
    phone_number.fetch.country_code
  rescue StandardError => e
    logger.error(error: 'Country Code parse error', message: e.message)
    raise e
  end

  # @param type [String]
  # @return [String]
  def sanitized(number)
    phone_number = client.lookups.phone_numbers(number)
    phone_number.fetch.phone_number
  rescue StandardError => e
    logger.error(error: 'Phone number parse error', message: e.message)
    raise e
  end
end
