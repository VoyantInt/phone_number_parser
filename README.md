###  How to use

`gem install 'twilio-ruby'`

`export TWILIO_ACCOUNT_SID=<See privnote>`
`export TWILIO_AUTH_TOKEN=<See privnote>`

```
ruby bin/main.rb 4145341207 +14145341207 497113804761 +27177123841
=>
US +14145341207
US +14145341207
DE +497113804761
ZA +27177123841
```

#### Reservations about my approach
I decided to use the Twilio Lookup API for this project, mainly because it's
a company related to Voyant.

A few limitations I found with the free version:

They offer country_code but not country_name.

One request per one phone number.

A limitation with my code is ARGV is delimited by space so (555) 555-1234 results in a defect.
